package eu.fiveminutes.droidcon.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import eu.fiveminutes.droidcon.demo.R;
import eu.fiveminutes.droidcon.demo.view.AnimatedDotWithQuadEasing;

public final class AnimatedDotWithQuadEasingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animated_dot_with_quad_easing);

        final AnimatedDotWithQuadEasing animation = (AnimatedDotWithQuadEasing) findViewById(R.id.animationView);

        findViewById(R.id.btnToggleAnimation).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                animation.toggle();
            }
        });
    }
}
