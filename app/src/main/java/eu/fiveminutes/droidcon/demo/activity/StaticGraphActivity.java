package eu.fiveminutes.droidcon.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import eu.fiveminutes.droidcon.demo.R;

public final class StaticGraphActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_graph);
    }
}
