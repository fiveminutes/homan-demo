package eu.fiveminutes.droidcon.demo.util;

import android.graphics.PointF;

public final class CoordinateUtils {

    public static PointF fromPolar(int r, int phi) {
        return fromPolar(r, phi, new PointF(0, 0));
    }

    public static PointF fromPolar(int r, int phi, PointF center) {
        final PointF point = new PointF();
        final double radians = Math.toRadians(phi);
        point.set((float) Math.cos(radians) * r + center.x, (float) -Math.sin(radians) * r + center.y);
        return point;
    }
}
