package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

import eu.fiveminutes.droidcon.demo.util.BezierUtils;

public final class StaticGraph extends View {

    private static final int NUMBER_OF_HELPER_AXES = 10;
    private static final float[] DATA_POINTS_COEFFICIENTS = new float[]{0.5f, -0.25f, 0.75f, -0.5f, 0.25f, -0.3f};

    private Paint axisPaint;
    private Paint helperAxesPaint;
    private Paint graphPaint;

    private PointF xAxisStart;
    private PointF xAxisEnd;

    private PointF yAxisStart;
    private PointF yAxisEnd;

    private PointF[] helperAxesStarts;
    private PointF[] helperAxesEnds;

    private PointF[] dataPoints;
    private Path graphPath;

    public StaticGraph(final Context context) {
        super(context);
        init();
    }

    public StaticGraph(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StaticGraph(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        axisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        axisPaint.setColor(Color.BLACK);
        axisPaint.setStyle(Paint.Style.STROKE);
        axisPaint.setStrokeWidth(4.0f);

        helperAxesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        helperAxesPaint.setColor(Color.BLACK);
        helperAxesPaint.setStyle(Paint.Style.STROKE);
        helperAxesPaint.setStrokeWidth(2.0f);

        graphPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        graphPaint.setColor(Color.GREEN);
        graphPaint.setStyle(Paint.Style.STROKE);
        graphPaint.setStrokeWidth(6.0f);
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        calculateAxis(width, height);
        calculateDataPoints(width, height);
    }

    private void calculateAxis(final int width, final int height) {

        xAxisStart = new PointF(0f, height / 2.0f);
        xAxisEnd = new PointF((float) width, height / 2.0f);

        yAxisStart = new PointF(width / 2.0f, 0.0f);
        yAxisEnd = new PointF(width / 2.0f, (float) height);

        helperAxesStarts = new PointF[NUMBER_OF_HELPER_AXES];
        helperAxesEnds = new PointF[NUMBER_OF_HELPER_AXES];
        final float helperAxisYStep = height / NUMBER_OF_HELPER_AXES;
        for (int i = 0; i < NUMBER_OF_HELPER_AXES; i++) {
            helperAxesStarts[i] = new PointF(0.0f, i * helperAxisYStep);
            helperAxesEnds[i] = new PointF((float) width, i * helperAxisYStep);
        }
    }

    private void calculateDataPoints(final int width, final int height) {

        final int numberOfDataPoints = DATA_POINTS_COEFFICIENTS.length + 1;
        final float dataPointsStep = width / (float) DATA_POINTS_COEFFICIENTS.length;
        final float heightHalf = height / 2.0f;

        /*
            Separate array with points is redundant, we could have calculate path directly,
            but keeping data separated tends to be handy
        */
        dataPoints = new PointF[numberOfDataPoints];
        dataPoints[0] = new PointF(0.0f, height / 2.0f);
        for (int i = 1; i < numberOfDataPoints; i++) {
            dataPoints[i] = new PointF(i * dataPointsStep, heightHalf - heightHalf * DATA_POINTS_COEFFICIENTS[i - 1]);
        }

        // Bezier
        graphPath = BezierUtils.calculateBezier(dataPoints);

        // Straight lines
//        graphPath = calculateLinePath(dataPoints, numberOfDataPoints);
    }

    private Path calculateLinePath(final PointF[] dataPoints, final int numberOfDataPoints) {
        final Path path = new Path();
        path.moveTo(dataPoints[0].x, dataPoints[0].y);
        for (int i = 1; i < numberOfDataPoints; i++) {
            path.lineTo(dataPoints[i].x, dataPoints[i].y);
        }
        return path;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawLine(xAxisStart.x, xAxisStart.y, xAxisEnd.x, xAxisEnd.y, axisPaint);
        canvas.drawLine(yAxisStart.x, yAxisStart.y, yAxisEnd.x, yAxisEnd.y, axisPaint);

        for (int i = 0; i < NUMBER_OF_HELPER_AXES; i++) {
            canvas.drawLine(helperAxesStarts[i].x, helperAxesStarts[i].y, helperAxesEnds[i].x, helperAxesEnds[i].y, helperAxesPaint);
        }

        canvas.drawPath(graphPath, graphPaint);
    }
}
