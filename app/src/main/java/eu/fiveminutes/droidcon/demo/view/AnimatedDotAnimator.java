package eu.fiveminutes.droidcon.demo.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

public final class AnimatedDotAnimator extends View {

    private static final long ANIMATION_DURATION_IN_MILLIS = 3000; // millis

    private Paint dotPaint;
    private Paint endPointPaint;

    private PointF startPoint;
    private PointF endPoint;

    private PointF currentPoint;

    private AnimatorSet animator;

    private boolean goingStartToEnd = true;

    public AnimatedDotAnimator(final Context context) {
        super(context);
        init();
    }

    public AnimatedDotAnimator(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimatedDotAnimator(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        dotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        dotPaint.setColor(Color.RED);
        dotPaint.setStyle(Paint.Style.FILL);
        dotPaint.setStrokeWidth(1.0f);

        endPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        endPointPaint.setColor(Color.GREEN);
        endPointPaint.setStyle(Paint.Style.FILL);
        endPointPaint.setStrokeWidth(1.0f);
    }

    public void toggle() {

        if (isStopped()) {
            startAnimating();
        } else if (isPaused()) {
            animator.resume();
        } else {
            animator.pause();
        }
    }

    private boolean isStopped() {
        return animator == null || !animator.isStarted();
    }

    private boolean isPaused() {
        return animator != null && animator.isPaused();
    }

    private void startAnimating() {

        final PointF endPoint;
        if (goingStartToEnd) {
            endPoint = this.endPoint;
        } else {
            endPoint = this.startPoint;
        }

        final ValueAnimator xAnimator = buildAnimator(currentPoint.x, endPoint.x);
        final ValueAnimator yAnimator = buildAnimator(currentPoint.y, endPoint.y);

        animator = new AnimatorSet();
        animator.play(xAnimator).with(yAnimator);

        xAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentPoint.x = (float) animation.getAnimatedValue();
                invalidate();
            }
        });

        yAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentPoint.y = (float) animation.getAnimatedValue();
                invalidate();
            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                goingStartToEnd = !goingStartToEnd;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private ValueAnimator buildAnimator(final float from, final float to) {
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(from, to);
        valueAnimator.setDuration(ANIMATION_DURATION_IN_MILLIS);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        return valueAnimator;
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        startPoint = new PointF(width / 4.0f, height * 3.0f / 4.0f);
        endPoint = new PointF(width * 3.0f / 4.0f, height / 4.0f);

        currentPoint = new PointF(startPoint.x, startPoint.y);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        drawDot(canvas, startPoint, endPointPaint);
        drawDot(canvas, endPoint, endPointPaint);


        if (currentPoint != null) {
            drawDot(canvas, currentPoint, dotPaint);
        }
    }

    private void drawDot(final Canvas canvas, final PointF point, final Paint paint) {
        canvas.drawCircle(point.x, point.y, 10.0f, paint);
    }
}
