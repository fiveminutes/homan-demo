package eu.fiveminutes.droidcon.demo.util;

import android.graphics.Path;
import android.graphics.PointF;

public final class BezierUtils {

    public static Path calculateBezier(PointF[] points, boolean closed) {
        PointF[] divided = CatmullRomSplineUtils.subdividePoints(points, 10, closed);
        final Path path = new Path();
        path.moveTo((float) divided[0].x, (float) divided[0].y);
        for (int i = 0; i < divided.length - 2; i++) {
            path.quadTo((float) divided[i + 1].x, (float) divided[i + 1].y, (float) divided[i + 2].x, (float) divided[i + 2].y);
        }
        return path;
    }

    public static Path calculateBezier(PointF[] points) {
        return calculateBezier(points, false);
    }

    public static PointF fromPolar(int r, int phi) {
        return fromPolar(r, phi, new PointF(0, 0));
    }

    public static PointF fromPolar(int r, int phi, PointF center) {
        final PointF point = new PointF();
        final double radians = Math.toRadians(phi);
        point.set((float) Math.cos(radians) * r + center.x, (float) -Math.sin(radians) * r + center.y);
        return point;
    }
}
