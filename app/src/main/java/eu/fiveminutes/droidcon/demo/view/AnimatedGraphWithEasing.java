package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

import java.util.concurrent.TimeUnit;

import eu.fiveminutes.droidcon.demo.util.BezierUtils;

public final class AnimatedGraphWithEasing extends View {

    private static final long UI_REFRESH_RATE = 100; // fps
    private static final long ANIMATION_REFRESHING_INTERVAL = TimeUnit.SECONDS.toMillis(1L) / UI_REFRESH_RATE; // millis
    private static final long ANIMATION_DURATION_IN_MILLIS = 800; // millis
    private static final long NUMBER_OF_FRAMES = ANIMATION_DURATION_IN_MILLIS / ANIMATION_REFRESHING_INTERVAL;

    private static final int NUMBER_OF_HELPER_AXES = 10;
    private static final float[] START_DATA_POINTS_COEFFICIENTS = new float[]{0.5f, -0.25f, 0.75f, -0.5f, 0.25f, -0.3f};
    private static final float[] END_DATA_POINTS_COEFFICIENTS = new float[]{0.25f, -0.5f, 0.2f, -0.5f, 0.75f, -0.5f};

    private static final float HELPER_AXES_START_ZOOM = 1.0f;
    private static final float HELPER_AXES_END_ZOOM = 2.0f;

    private static final int START_GRAPH_COLOR = Color.GREEN;
    private static final int END_GRAPH_COLOR = Color.RED;

    private final Handler uiHandler = new Handler(Looper.getMainLooper());

    private Paint axisPaint;
    private Paint helperAxesPaint;
    private Paint graphPaint;

    private PointF xAxisStart;
    private PointF xAxisEnd;

    private PointF yAxisStart;
    private PointF yAxisEnd;

    private PointF[] helperAxesStarts;
    private PointF[] helperAxesEnds;

    private PointF[] startDataPoints;
    private PointF[] endDataPoints;

    private PointF[][] framesDataPoints;
    private float[] framesAxisZoom;
    private int[] framesColor;

    private int viewPortHeight;

    private boolean isAnimating = false;
    private boolean goingStartToEnd = true;
    private int currentFrame;

    public AnimatedGraphWithEasing(final Context context) {
        super(context);
        init();
    }

    public AnimatedGraphWithEasing(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimatedGraphWithEasing(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        axisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        axisPaint.setColor(Color.BLACK);
        axisPaint.setStyle(Paint.Style.STROKE);
        axisPaint.setStrokeWidth(4.0f);

        helperAxesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        helperAxesPaint.setColor(Color.BLACK);
        helperAxesPaint.setStyle(Paint.Style.STROKE);
        helperAxesPaint.setStrokeWidth(2.0f);

        graphPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        graphPaint.setColor(Color.BLACK);
        graphPaint.setStyle(Paint.Style.STROKE);
        graphPaint.setStrokeWidth(6.0f);
    }

    public void toggle() {
        if (isAnimating) {
            stopAnimating();
        } else {
            startAnimating();
        }

        isAnimating = !isAnimating;
    }

    private void startAnimating() {
        calculateFrames();
        uiHandler.post(invalidateUI);
    }

    private void stopAnimating() {
        uiHandler.removeCallbacks(invalidateUI);
    }

    private Runnable invalidateUI = new Runnable() {

        @Override
        public void run() {
            if (hasFrameToDraw()) {
                invalidate();
                uiHandler.postDelayed(this, ANIMATION_REFRESHING_INTERVAL);
            } else {
                isAnimating = false;
            }
        }
    };

    private void calculateFrames() {

        if (hasFrameToDraw()) {
            return;
        }

        final int numberOfDataPoints = startDataPoints.length;
        framesDataPoints = new PointF[(int) NUMBER_OF_FRAMES + 1][numberOfDataPoints];
        framesAxisZoom = new float[(int) NUMBER_OF_FRAMES + 1];
        framesColor = new int[(int) NUMBER_OF_FRAMES + 1];

        calculateDataPointsFrames(framesDataPoints, numberOfDataPoints);
        calculateAxisZoomFrames(framesAxisZoom);
        calculateColorFrames(framesColor);

        currentFrame = 0;
        goingStartToEnd = !goingStartToEnd;
    }

    private void calculateDataPointsFrames(final PointF[][] frames, final int numberOfDataPoints) {
        final PointF[] animationStartPoints;
        final PointF[] animationEndPoints;

        if (goingStartToEnd) {
            animationStartPoints = startDataPoints;
            animationEndPoints = endDataPoints;
        } else {
            animationStartPoints = endDataPoints;
            animationEndPoints = startDataPoints;
        }

        final float[] aY = new float[numberOfDataPoints];
        final float[] bY = new float[numberOfDataPoints];
        final boolean[] directions = new boolean[numberOfDataPoints];

        for (int i = 0; i < numberOfDataPoints; i++) {
            final Pair<Float, Float> coefficients = calculateInterpolatorCoefficients(Math.abs(animationEndPoints[i].y - animationStartPoints[i].y), NUMBER_OF_FRAMES);
            aY[i] = coefficients.first;
            bY[i] = coefficients.second;
            directions[i] = animationEndPoints[i].y - animationStartPoints[i].y < 0;
        }

        for (int i = 0; i < numberOfDataPoints; i++) {

            float x = animationStartPoints[i].x;
            for (int j = 0; j < NUMBER_OF_FRAMES; j++) {
                final float y = calculateFunction(aY[i], bY[i], j, animationStartPoints[i].y, directions[i]);
                frames[j][i] = new PointF(x, y);
            }
        }

        for (int i = 0; i < numberOfDataPoints; i++) {
            frames[frames.length - 1][i] = new PointF(animationEndPoints[i].x, animationEndPoints[i].y);
        }
    }

    private float calculateFunction(final float a, final float b, final int i, final float origin, final boolean flipSigns) {
        final int sign = flipSigns ? -1 : 1;
        return sign * (a * i * i * i + b * i * i) + origin;
    }

    private Pair<Float, Float> calculateInterpolatorCoefficients(final float pathLength, final long numberOfPoints) {
        return new Pair<>(-2 * pathLength / (numberOfPoints * numberOfPoints * numberOfPoints), 3 * pathLength / (numberOfPoints * numberOfPoints));
    }

    private void calculateAxisZoomFrames(final float[] frames) {

        final float startZoom = goingStartToEnd ? HELPER_AXES_START_ZOOM : HELPER_AXES_END_ZOOM;
        final float endZoom = goingStartToEnd ? HELPER_AXES_END_ZOOM : HELPER_AXES_START_ZOOM;
        final Pair<Float, Float> coefficients = calculateInterpolatorCoefficients(endZoom - startZoom, NUMBER_OF_FRAMES);

        for (int i = 0; i < frames.length; i++) {
            frames[i] = calculateFunction(coefficients.first, coefficients.second, i, startZoom, false);
        }

        frames[frames.length - 1] = endZoom;
    }

    private void calculateColorFrames(final int[] frames) {

        final int startColorRed = goingStartToEnd ? Color.red(START_GRAPH_COLOR) : Color.red(END_GRAPH_COLOR);
        final int startColorGreen = goingStartToEnd ? Color.green(START_GRAPH_COLOR) : Color.green(END_GRAPH_COLOR);
        final int startColorBlue = goingStartToEnd ? Color.blue(START_GRAPH_COLOR) : Color.blue(END_GRAPH_COLOR);

        final int endColorRed = goingStartToEnd ? Color.red(END_GRAPH_COLOR) : Color.red(START_GRAPH_COLOR);
        final int endColorGreen = goingStartToEnd ? Color.green(END_GRAPH_COLOR) : Color.green(START_GRAPH_COLOR);
        final int endColorBlue = goingStartToEnd ? Color.blue(END_GRAPH_COLOR) : Color.blue(START_GRAPH_COLOR);

        final float redStep = (endColorRed - startColorRed) / (float) NUMBER_OF_FRAMES;
        final float greenStep = (endColorGreen - startColorGreen) / (float) NUMBER_OF_FRAMES;
        final float blueStep = (endColorBlue - startColorBlue) / (float) NUMBER_OF_FRAMES;

        for (int i = 0; i < frames.length; i++) {
            frames[i] = Color.rgb(
                    startColorRed + (int) (i * redStep),
                    startColorGreen + (int) (i * greenStep),
                    startColorBlue + (int) (i * blueStep));
        }

        frames[frames.length - 1] = Color.rgb(endColorRed, endColorGreen, endColorBlue);
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        viewPortHeight = height;

        calculateAxis(width, height);
        calculateDataPoints(width, height);
    }

    private void calculateAxis(final int width, final int height) {

        xAxisStart = new PointF(0f, height / 2.0f);
        xAxisEnd = new PointF((float) width, height / 2.0f);

        yAxisStart = new PointF(width / 2.0f, 0.0f);
        yAxisEnd = new PointF(width / 2.0f, (float) height);

        helperAxesStarts = new PointF[NUMBER_OF_HELPER_AXES];
        helperAxesEnds = new PointF[NUMBER_OF_HELPER_AXES];
        final float helperAxisYStep = height / NUMBER_OF_HELPER_AXES;
        for (int i = 0; i < NUMBER_OF_HELPER_AXES; i++) {
            helperAxesStarts[i] = new PointF(0.0f, i * helperAxisYStep);
            helperAxesEnds[i] = new PointF((float) width, i * helperAxisYStep);
        }
    }

    private void calculateDataPoints(final int width, final int height) {

        final int numberOfDataPoints = START_DATA_POINTS_COEFFICIENTS.length + 1;
        final float dataPointsStep = width / (float) START_DATA_POINTS_COEFFICIENTS.length;
        final float heightHalf = height / 2.0f;

        startDataPoints = new PointF[numberOfDataPoints];
        endDataPoints = new PointF[numberOfDataPoints];
        startDataPoints[0] = new PointF(0.0f, height / 2.0f);
        endDataPoints[0] = new PointF(0.0f, height / 2.0f);
        for (int i = 1; i < numberOfDataPoints; i++) {
            startDataPoints[i] = new PointF(i * dataPointsStep, heightHalf - heightHalf * START_DATA_POINTS_COEFFICIENTS[i - 1]);
            endDataPoints[i] = new PointF(i * dataPointsStep, heightHalf - heightHalf * END_DATA_POINTS_COEFFICIENTS[i - 1]);
        }
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawLine(xAxisStart.x, xAxisStart.y, xAxisEnd.x, xAxisEnd.y, axisPaint);
        canvas.drawLine(yAxisStart.x, yAxisStart.y, yAxisEnd.x, yAxisEnd.y, axisPaint);

        if (!hasFrameToDraw()) {
            graphPaint.setColor(START_GRAPH_COLOR);
            drawHelperAxes(canvas, HELPER_AXES_START_ZOOM);
            canvas.drawPath(BezierUtils.calculateBezier(startDataPoints), graphPaint);
            return;
        }

        drawHelperAxes(canvas, framesAxisZoom[currentFrame]);

        graphPaint.setColor(framesColor[currentFrame]);
        canvas.drawPath(BezierUtils.calculateBezier(framesDataPoints[currentFrame]), graphPaint);
//        canvas.drawPath(BezierUtils.calculateBezier(startDataPoints), axisPaint);
//        canvas.drawPath(BezierUtils.calculateBezier(endDataPoints), axisPaint);
        currentFrame++;
    }

    void drawHelperAxes(final Canvas canvas, final float zoom) {

        final float heightHalf = viewPortHeight / 2.0f;
        final float epsilon = 10.0f;

        for (int i = 0; i < NUMBER_OF_HELPER_AXES; i++) {

            final float newYStart = (heightHalf - helperAxesStarts[i].y) * zoom + heightHalf;
            final float newYEnd = (heightHalf - helperAxesEnds[i].y) * zoom + heightHalf;

            if (biggerThanEpsilon(newYStart, heightHalf, epsilon) && biggerThanEpsilon(newYEnd, heightHalf, epsilon)) {
                canvas.drawLine(helperAxesStarts[i].x, newYStart, helperAxesEnds[i].x, newYEnd, helperAxesPaint);
            }
        }
    }

    private boolean biggerThanEpsilon(final float a, final float b, final float epsilon) {
        return Math.abs(a - b) > epsilon;
    }

    private boolean hasFrameToDraw() {
        return framesDataPoints != null && currentFrame < framesDataPoints.length;
    }
}
