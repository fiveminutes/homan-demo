package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;

import java.util.concurrent.TimeUnit;

public final class BouncingDotWithGravity extends View {

    private static final long UI_REFRESH_RATE = 100; // fps
    private static final long ANIMATION_REFRESHING_INTERVAL = TimeUnit.SECONDS.toMillis(1L) / UI_REFRESH_RATE; // millis

    private static final PointF STARTING_POSITION = new PointF(100.0f, 100.0f);
    private static final PointF STARTING_VELOCITY = new PointF(15.0f, -20.0f);

    private static final PointF GRAVITY = new PointF(0.0f, 5.0f);

    private static final float WALL_THICKNESS = 10.0f;

    private final Handler uiHandler = new Handler(Looper.getMainLooper());

    private Paint dotPaint;
    private Paint wallsPaint;

    private boolean isAnimating = false;

    private PointF topLeft;
    private PointF topRight;
    private PointF bottomLeft;
    private PointF bottomRight;

    private PointF currentPosition = new PointF(STARTING_POSITION.x, STARTING_POSITION.y);
    private PointF currentVelocity = new PointF(STARTING_VELOCITY.x, STARTING_VELOCITY.y); // PointF because of x and y components - vector

    public BouncingDotWithGravity(final Context context) {
        super(context);
        init();
    }

    public BouncingDotWithGravity(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BouncingDotWithGravity(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        dotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        dotPaint.setColor(Color.RED);
        dotPaint.setStyle(Paint.Style.FILL);
        dotPaint.setStrokeWidth(1.0f);

        wallsPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        wallsPaint.setColor(Color.BLUE);
        wallsPaint.setStyle(Paint.Style.STROKE);
        wallsPaint.setStrokeWidth(WALL_THICKNESS);
    }

    public void toggle() {
        if (isAnimating) {
            stopAnimating();
        } else {
            startAnimating();
        }

        isAnimating = !isAnimating;
    }

    private void startAnimating() {
        uiHandler.post(invalidateUI);
    }

    private void stopAnimating() {
        uiHandler.removeCallbacks(invalidateUI);
    }

    private Runnable invalidateUI = new Runnable() {

        @Override
        public void run() {
            invalidate();
            uiHandler.postDelayed(this, ANIMATION_REFRESHING_INTERVAL);
        }
    };

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        topLeft = new PointF(0.0f, 0.0f);
        topRight = new PointF(width, 0.0f);
        bottomLeft = new PointF(0.0f, height);
        bottomRight = new PointF(width, height);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y, wallsPaint);
        drawDot(canvas, currentPosition, dotPaint);

        if (isAnimating) {
            updateWorld();
        }
    }

    private void drawDot(final Canvas canvas, final PointF point, final Paint paint) {
        canvas.drawCircle(point.x, point.y, 10.0f, paint);
    }

    private void updateWorld() {

        final float dvx = GRAVITY.x;
        final float dvy = GRAVITY.y;

        currentVelocity.set(currentVelocity.x + dvx, currentVelocity.y + dvy);

        final float dx = currentVelocity.x; // * dt
        final float dy = currentVelocity.y; // * dt

        currentPosition.set(currentPosition.x + dx, currentPosition.y + dy);

        if (hitRightWall()) {
            currentVelocity.x = -currentVelocity.x;
            currentPosition.set(topRight.x - WALL_THICKNESS, currentPosition.y);
        }

        if (hitLeftWall()) {
            currentVelocity.x = -currentVelocity.x;
            currentPosition.set(topLeft.x + WALL_THICKNESS, currentPosition.y);
        }

        if (hitTopWall()) {
            currentVelocity.y = -currentVelocity.y;
            currentPosition.set(currentPosition.x, topLeft.y + WALL_THICKNESS);
        }

        if (hitBottomWall()) {
            currentVelocity.y = -currentVelocity.y - GRAVITY.y; // Correction, otherwise we loose energy
            currentPosition.set(currentPosition.x, bottomLeft.y - WALL_THICKNESS);
        }
    }

    private boolean hitRightWall() {
        return currentPosition.x >= topRight.x - WALL_THICKNESS;
    }

    private boolean hitLeftWall() {
        return currentPosition.x <= topLeft.x + WALL_THICKNESS;
    }

    private boolean hitTopWall() {
        return currentPosition.y <= topLeft.y + WALL_THICKNESS;
    }

    private boolean hitBottomWall() {
        return currentPosition.y >= bottomLeft.y - WALL_THICKNESS;
    }
}
