package eu.fiveminutes.droidcon.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import eu.fiveminutes.droidcon.demo.R;
import eu.fiveminutes.droidcon.demo.view.AnimatedDot;

public final class AnimatedDotActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animated_dot);

        final AnimatedDot animation = (AnimatedDot) findViewById(R.id.animationView);

        findViewById(R.id.btnToggleAnimation).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                animation.toggle();
            }
        });
    }
}
