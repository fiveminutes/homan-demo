package eu.fiveminutes.droidcon.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import eu.fiveminutes.droidcon.demo.R;
import eu.fiveminutes.droidcon.demo.view.AnimatedDotAnimator;

public final class AnimatedDotAnimatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animated_dot_animator);

        final AnimatedDotAnimator animation = (AnimatedDotAnimator) findViewById(R.id.animationView);

        findViewById(R.id.btnToggleAnimation).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                animation.toggle();
            }
        });
    }
}
