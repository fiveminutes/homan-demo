package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;

import java.util.concurrent.TimeUnit;

public final class AnimatedDotWithCubeEasing extends View {

    private static final long UI_REFRESH_RATE = 100; // fps
    private static final long ANIMATION_REFRESHING_INTERVAL = TimeUnit.SECONDS.toMillis(1L) / UI_REFRESH_RATE; // millis
    private static final long ANIMATION_DURATION_IN_MILLIS = 1500; // millis
    private static final long NUMBER_OF_FRAMES = ANIMATION_DURATION_IN_MILLIS / ANIMATION_REFRESHING_INTERVAL;

    private final Handler uiHandler = new Handler(Looper.getMainLooper());

    private Paint dotPaint;
    private Paint endPointPaint;

    private PointF startPoint;
    private PointF endPoint;

    private boolean isAnimating = false;
    private boolean goingStartToEnd = true;

    private PointF[] frames;
    private int currentFrame;

    public AnimatedDotWithCubeEasing(final Context context) {
        super(context);
        init();
    }

    public AnimatedDotWithCubeEasing(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimatedDotWithCubeEasing(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        dotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        dotPaint.setColor(Color.RED);
        dotPaint.setStyle(Paint.Style.FILL);
        dotPaint.setStrokeWidth(1.0f);

        endPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        endPointPaint.setColor(Color.GREEN);
        endPointPaint.setStyle(Paint.Style.FILL);
        endPointPaint.setStrokeWidth(1.0f);
    }

    public void toggle() {
        if (isAnimating) {
            stopAnimating();
        } else {
            startAnimating();
        }

        isAnimating = !isAnimating;
    }

    private void startAnimating() {
        calculateFrames();
        uiHandler.post(invalidateUI);
    }

    private void stopAnimating() {
        uiHandler.removeCallbacks(invalidateUI);
    }

    private Runnable invalidateUI = new Runnable() {

        @Override
        public void run() {
            if (hasFrameToDraw()) {
                invalidate();
                uiHandler.postDelayed(this, ANIMATION_REFRESHING_INTERVAL);
            } else {
                isAnimating = false;
            }
        }
    };

    private void calculateFrames() {

        if (hasFrameToDraw()) {
            return;
        }

        /*
            For cubic interpolation the formula is:
            x(i) = (-2 * L / (n * n * n)) * (i * i * i) + ((3 * L) / (n * n)) * i * i
            where L is the length of the path, n is number of frames and i is discrete unit, goes from 0 to n

            Switching:
            aX = -2 * L / (n * n * n) and bX = (3 * L) / (n * n)
            x(i) = aX * i * i * i + bX * i * i

            The formula assumes origin is (0, 0), so we have to translate by starting position

            x(i) = aX * i * i * i + bX * i * i + startX

            It is similar with Y coordinate, but we have to switch signs because of Android screen coordinates
            y(i) = -(aY * i * i * i + bY * i * i) + startY

            And again similarly, we switch signs for going from the end point to the start point
         */

        final float pathLengthX = Math.abs(endPoint.x - startPoint.x);
        final float pathLengthY = Math.abs(endPoint.y - startPoint.y);

        frames = new PointF[(int) NUMBER_OF_FRAMES + 1];

        final PointF animationStartPoint;
        final PointF animationEndPoint;

        if (goingStartToEnd) {
            animationStartPoint = startPoint;
            animationEndPoint = endPoint;
        } else {
            animationStartPoint = endPoint;
            animationEndPoint = startPoint;
        }

        final float aX = -2 * pathLengthX / (NUMBER_OF_FRAMES * NUMBER_OF_FRAMES * NUMBER_OF_FRAMES);
        final float bX = 3 * pathLengthX / (NUMBER_OF_FRAMES * NUMBER_OF_FRAMES);

        final float aY = -2 * pathLengthY / (NUMBER_OF_FRAMES * NUMBER_OF_FRAMES * NUMBER_OF_FRAMES);
        final float bY = 3 * pathLengthY / (NUMBER_OF_FRAMES * NUMBER_OF_FRAMES);

        for (int i = 0; i < NUMBER_OF_FRAMES; i++) {

            final float x = calculateFunction(aX, bX, i, animationStartPoint.x, !goingStartToEnd);
            final float y = calculateFunction(aY, bY, i, animationStartPoint.y, goingStartToEnd);

            frames[i] = new PointF(x, y);
        }

        frames[frames.length - 1] = new PointF(animationEndPoint.x, animationEndPoint.y);

        currentFrame = 0;
        goingStartToEnd = !goingStartToEnd;
    }

    private float calculateFunction(final float a, final float b, final int i, final float origin, final boolean flipSigns) {
        final int sign = flipSigns ? -1 : 1;
        return sign * (a * i * i * i + b * i * i) + origin;
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        startPoint = new PointF(width / 4.0f, height * 3.0f / 4.0f);
        endPoint = new PointF(width * 3.0f / 4.0f, height / 4.0f);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        drawDot(canvas, startPoint, endPointPaint);
        drawDot(canvas, endPoint, endPointPaint);

        if (!hasFrameToDraw()) {
            drawDot(canvas, startPoint, dotPaint);
            return;
        }

        final PointF currentPoint = frames[currentFrame];
        drawDot(canvas, currentPoint, dotPaint);

        currentFrame++;
    }

    private boolean hasFrameToDraw() {
        return frames != null && currentFrame < frames.length;
    }

    private void drawDot(final Canvas canvas, final PointF point, final Paint paint) {
        canvas.drawCircle(point.x, point.y, 10.0f, paint);
    }
}
