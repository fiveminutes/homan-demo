package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import eu.fiveminutes.droidcon.demo.util.BezierUtils;
import eu.fiveminutes.droidcon.demo.util.CoordinateUtils;

public final class PulsingCircle extends View {

    private static final long UI_REFRESH_RATE = 100; // fps
    private static final long ANIMATION_REFRESHING_INTERVAL = TimeUnit.SECONDS.toMillis(1L) / UI_REFRESH_RATE; // millis

    private static final int NUMBER_OD_CONTROL_POINTS = 8;
    private static final float SPRING_CONSTANT = 0.015f;

    private final Handler uiHandler = new Handler(Looper.getMainLooper());
    private final Random random = new Random();

    private Paint circlePaint;
    private Paint controlPointPaint;
    private Paint graphPaint;

    private PointF centerPosition;
    private float radius;

    private PointF[] controlPoints;
    private PointF[] controlPointsVelocities;

    private boolean isAnimating = false;

    public PulsingCircle(final Context context) {
        super(context);
        init();
    }

    public PulsingCircle(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PulsingCircle(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(Color.GREEN);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeWidth(5.0f);

        controlPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        controlPointPaint.setColor(Color.RED);
        controlPointPaint.setStyle(Paint.Style.FILL);
        controlPointPaint.setStrokeWidth(1.0f);

        graphPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        graphPaint.setColor(Color.BLUE);
        graphPaint.setStyle(Paint.Style.STROKE);
        graphPaint.setStrokeWidth(5.0f);
    }

    public void toggle() {
        if (isAnimating) {
            stopAnimating();
        } else {
            startAnimating();
        }

        isAnimating = !isAnimating;
    }

    private void startAnimating() {
        uiHandler.post(invalidateUI);
    }

    private void stopAnimating() {
        uiHandler.removeCallbacks(invalidateUI);
    }

    private Runnable invalidateUI = new Runnable() {

        @Override
        public void run() {
            invalidate();
            uiHandler.postDelayed(this, ANIMATION_REFRESHING_INTERVAL);
        }
    };

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        centerPosition = new PointF(width / 2.0f, height / 2.0f);
        radius = width / 4.0f;

        controlPoints = new PointF[NUMBER_OD_CONTROL_POINTS + 1];
        final int angularStep = 360 / NUMBER_OD_CONTROL_POINTS;
        for (int i = 0; i < NUMBER_OD_CONTROL_POINTS; i++) {
            final int angle = i * angularStep;
            controlPoints[i] = CoordinateUtils.fromPolar((int) ((random.nextFloat() - 0.5f) * radius + radius), angle, centerPosition);
            controlPoints[controlPoints.length - 1] = new PointF(controlPoints[0].x, controlPoints[0].y);
        }

        controlPointsVelocities = new PointF[NUMBER_OD_CONTROL_POINTS + 1];
        for (int i = 0; i < controlPointsVelocities.length; i++) {
            controlPointsVelocities[i] = new PointF();
        }
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(centerPosition.x, centerPosition.y, radius, circlePaint);
        drawControlPoints(controlPoints, canvas, controlPointPaint);
        canvas.drawPath(BezierUtils.calculateBezier(controlPoints, true), graphPaint);

        if (isAnimating) {
            updateWorld();
        }
    }

    private void drawControlPoints(final PointF[] points, final Canvas canvas, final Paint paint) {
        for (int i = 0; i < points.length; i++) {
            drawDot(canvas, points[i], paint);
        }
    }

    private void drawDot(final Canvas canvas, final PointF point, final Paint paint) {
        canvas.drawCircle(point.x, point.y, 10.0f, paint);
    }

    private void updateWorld() {

        final int angleStep = 360 / NUMBER_OD_CONTROL_POINTS;
        for (int i = 0; i < controlPoints.length; i++) {

            final PointF point = controlPoints[i];
            final PointF velocity = controlPointsVelocities[i];

            final PointF springCenter = CoordinateUtils.fromPolar((int) radius, i * angleStep, centerPosition);
            final float forceX = -SPRING_CONSTANT * (point.x - springCenter.x);
            final float forceY = -SPRING_CONSTANT * (point.y - springCenter.y);

            final float dvx = forceX;
            final float dvy = forceY;

            velocity.set(velocity.x + dvx, velocity.y + dvy);

            final float dx = velocity.x;
            final float dy = velocity.y;

            point.set(point.x + dx, point.y + dy);
        }
    }
}
