package eu.fiveminutes.droidcon.demo.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import eu.fiveminutes.droidcon.demo.util.BezierUtils;

public final class GraphAnimator extends View {

    private static final long ANIMATION_DURATION_IN_MILLIS = 1500; // millis

    private static final float HELPER_AXES_START_ZOOM = 1.0f;
    private static final float HELPER_AXES_END_ZOOM = 2.0f;

    private static final int START_GRAPH_COLOR = Color.GREEN;
    private static final int END_GRAPH_COLOR = Color.RED;

    private static final float[] START_DATA_POINTS_COEFFICIENTS = new float[]{0.5f, -0.25f, 0.75f, -0.5f, 0.25f, -0.3f};
    private static final float[] END_DATA_POINTS_COEFFICIENTS = new float[]{0.25f, -0.5f, 0.2f, -0.5f, 0.75f, -0.5f};

    private Paint axisPaint;
    private Paint helperAxesPaint;
    private Paint graphPaint;

    private Graph startingGraph;
    private Graph endingGraph;

    private Graph currentGraph;
    private GraphState currentGraphState;

    private ObjectAnimator animator;

    private boolean goingStartToEnd = true;

    public GraphAnimator(final Context context) {
        super(context);
        init();
    }

    public GraphAnimator(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GraphAnimator(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        axisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        axisPaint.setColor(Color.BLACK);
        axisPaint.setStyle(Paint.Style.STROKE);
        axisPaint.setStrokeWidth(4.0f);

        helperAxesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        helperAxesPaint.setColor(Color.BLACK);
        helperAxesPaint.setStyle(Paint.Style.STROKE);
        helperAxesPaint.setStrokeWidth(2.0f);

        graphPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        graphPaint.setColor(Color.BLACK);
        graphPaint.setStyle(Paint.Style.STROKE);
        graphPaint.setStrokeWidth(6.0f);
    }

    public void toggle() {

        if (isStopped()) {
            startAnimating();
        } else if (isPaused()) {
            animator.resume();
        } else {
            animator.pause();
        }
    }

    private boolean isStopped() {
        return animator == null || !animator.isStarted();
    }

    private boolean isPaused() {
        return animator != null && animator.isPaused();
    }

    private void startAnimating() {

        final GraphState currentGraphState;
        if (goingStartToEnd) {
            currentGraphState = new GraphState(startingGraph, endingGraph);
        } else {
            currentGraphState = new GraphState(endingGraph, startingGraph);
        }

        animator = ObjectAnimator.ofFloat(currentGraphState, "fraction", 0.0f, 1.0f);
        animator.setDuration(ANIMATION_DURATION_IN_MILLIS);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentGraph.updateState(currentGraphState);
                invalidate();
            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                goingStartToEnd = !goingStartToEnd;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        final int numberOfDataPoints = START_DATA_POINTS_COEFFICIENTS.length + 1;
        final float dataPointsStep = width / (float) START_DATA_POINTS_COEFFICIENTS.length;
        final float heightHalf = height / 2.0f;

        final PointF[] startDataPoints = new PointF[numberOfDataPoints];
        final PointF[] endDataPoints = new PointF[numberOfDataPoints];
        startDataPoints[0] = new PointF(0.0f, height / 2.0f);
        endDataPoints[0] = new PointF(0.0f, height / 2.0f);
        for (int i = 1; i < numberOfDataPoints; i++) {
            startDataPoints[i] = new PointF(i * dataPointsStep, heightHalf - heightHalf * START_DATA_POINTS_COEFFICIENTS[i - 1]);
            endDataPoints[i] = new PointF(i * dataPointsStep, heightHalf - heightHalf * END_DATA_POINTS_COEFFICIENTS[i - 1]);
        }

        startingGraph = new Graph(axisPaint, graphPaint, helperAxesPaint,
                width, height, startDataPoints, START_GRAPH_COLOR, HELPER_AXES_START_ZOOM);

        endingGraph = new Graph(axisPaint, graphPaint, helperAxesPaint,
                width, height, endDataPoints, END_GRAPH_COLOR, HELPER_AXES_END_ZOOM);

        currentGraph = new Graph(axisPaint, graphPaint, helperAxesPaint,
                width, height, startDataPoints, START_GRAPH_COLOR, HELPER_AXES_START_ZOOM);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        currentGraph.draw(canvas);
    }

    private static final class Graph {

        private static final int NUMBER_OF_HELPER_AXES = 10;

        private final Paint axisPaint;
        private final Paint graphPaint;
        private Paint helperAxesPaint;

        // Fixed properties
        final PointF xAxisStart;
        final PointF xAxisEnd;
        final PointF yAxisStart;
        final PointF yAxisEnd;

        private final PointF[] helperAxesStarts;
        private final PointF[] helperAxesEnds;

        private final int height;

        // Animated properties
        private PointF[] controlPoints;
        private int color;
        private float zoom;

        private Graph(Paint axisPaint, Paint graphPaint, Paint helperAxesPaint,
                      int width, int height,
                      PointF[] controlPoints, int color, float zoom) {

            this.axisPaint = axisPaint;
            this.graphPaint = graphPaint;
            this.helperAxesPaint = helperAxesPaint;
            this.height = height;
            this.controlPoints = controlPoints;
            this.color = color;
            this.zoom = zoom;

            final float widthHalf = width / 2.0f;
            final float heightHalf = height / 2.0f;

            xAxisStart = new PointF(0f, heightHalf);
            xAxisEnd = new PointF((float) width, heightHalf);

            yAxisStart = new PointF(widthHalf, 0.0f);
            yAxisEnd = new PointF(widthHalf, (float) height);

            helperAxesStarts = new PointF[NUMBER_OF_HELPER_AXES];
            helperAxesEnds = new PointF[NUMBER_OF_HELPER_AXES];

            final float helperAxisYStep = height / NUMBER_OF_HELPER_AXES;
            for (int i = 0; i < NUMBER_OF_HELPER_AXES; i++) {
                helperAxesStarts[i] = new PointF(0.0f, i * helperAxisYStep);
                helperAxesEnds[i] = new PointF((float) width, i * helperAxisYStep);
            }
        }

        public void updateState(final GraphState graphState) {
            controlPoints = graphState.getControlPoints();
            color = graphState.getColor();
            zoom = graphState.getZoom();
        }

        public void draw(final Canvas canvas) {
            canvas.drawLine(xAxisStart.x, xAxisStart.y, xAxisEnd.x, xAxisEnd.y, axisPaint);
            canvas.drawLine(yAxisStart.x, yAxisStart.y, yAxisEnd.x, yAxisEnd.y, axisPaint);

            drawHelperAxes(canvas, zoom);

            graphPaint.setColor(color);
            canvas.drawPath(BezierUtils.calculateBezier(controlPoints), graphPaint);
//        canvas.drawPath(BezierUtils.calculateBezier(startDataPoints), axisPaint);
//        canvas.drawPath(BezierUtils.calculateBezier(endDataPoints), axisPaint);
        }

        private void drawHelperAxes(final Canvas canvas, final float zoom) {

            final float heightHalf = height / 2.0f;
            final float epsilon = 10.0f;

            for (int i = 0; i < helperAxesStarts.length; i++) {

                final float newYStart = (heightHalf - helperAxesStarts[i].y) * zoom + heightHalf;
                final float newYEnd = (heightHalf - helperAxesStarts[i].y) * zoom + heightHalf;

                if (biggerThanEpsilon(newYStart, heightHalf, epsilon) && biggerThanEpsilon(newYEnd, heightHalf, epsilon)) {
                    canvas.drawLine(helperAxesStarts[i].x, newYStart, helperAxesEnds[i].x, newYEnd, helperAxesPaint);
                }
            }
        }

        private boolean biggerThanEpsilon(final float a, final float b, final float epsilon) {
            return Math.abs(a - b) > epsilon;
        }
    }

    private static class GraphState {
        private final PointF[] startingControlPoints;
        private final PointF[] endingControlPoints;

        private final int startingRedColor;
        private final int endingRedColor;

        private final int startingGreenColor;
        private final int endingGreenColor;

        private final int startingBlueColor;
        private final int endingBlueColor;

        private final float startingZoom;
        private final float endingZoom;

        private float fraction = 0.0f;

        private GraphState(Graph startingGraph, Graph endingGraph) {

            this.startingControlPoints = startingGraph.controlPoints;
            this.endingControlPoints = endingGraph.controlPoints;

            this.startingRedColor = Color.red(startingGraph.color);
            this.endingRedColor = Color.red(endingGraph.color);
            this.startingGreenColor = Color.green(startingGraph.color);
            this.endingGreenColor = Color.green(endingGraph.color);
            this.startingBlueColor = Color.blue(startingGraph.color);
            this.endingBlueColor = Color.blue(endingGraph.color);

            this.startingZoom = startingGraph.zoom;
            this.endingZoom = endingGraph.zoom;
        }

        public void setFraction(final float fraction) {
            this.fraction = fraction;
        }

        public PointF[] getControlPoints() {
            final PointF[] controlPoints = new PointF[startingControlPoints.length];
            for (int i = 0; i < controlPoints.length; i++) {
                final float x = (endingControlPoints[i].x - startingControlPoints[i].x) * fraction + startingControlPoints[i].x;
                final float y = (endingControlPoints[i].y - startingControlPoints[i].y) * fraction + startingControlPoints[i].y;
                controlPoints[i] = new PointF(x, y);
            }
            return controlPoints;
        }

        public int getColor() {
            return Color.rgb(
                    (int) ((endingRedColor - startingRedColor) * fraction) + startingRedColor,
                    (int) ((endingGreenColor - startingGreenColor) * fraction) + startingGreenColor,
                    (int) ((endingBlueColor - startingBlueColor) * fraction) + startingBlueColor
            );
        }

        public float getZoom() {
            return (endingZoom - startingZoom) * fraction + startingZoom;
        }
    }
}
