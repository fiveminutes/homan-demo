package eu.fiveminutes.droidcon.demo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import eu.fiveminutes.droidcon.demo.R;

public final class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assignStartActivityActionToView(R.id.btnAnimatedDot, AnimatedDotActivity.class);
        assignStartActivityActionToView(R.id.btnAnimatedDotWithQuadEasing, AnimatedDotWithQuadEasingActivity.class);
        assignStartActivityActionToView(R.id.btnAnimatedDotWithCubeEasing, AnimatedDotWithCubeEasingActivity.class);
        assignStartActivityActionToView(R.id.btnAnimatedDotAnimator, AnimatedDotAnimatorActivity.class);
        assignStartActivityActionToView(R.id.btnAnimatedGraph, AnimatedGraphActivity.class);
        assignStartActivityActionToView(R.id.btnAnimatedGraphWithEasing, AnimatedGraphWithEasingActivity.class);
        assignStartActivityActionToView(R.id.btnGraphAnimator, GraphAnimatorActivity.class);
        assignStartActivityActionToView(R.id.btnBouncingDot, BouncingDotActivity.class);
        assignStartActivityActionToView(R.id.btnBouncingDotWithGravity, BouncingDotWithGravityActivity.class);
        assignStartActivityActionToView(R.id.btnEqualizer, EqualizerActivity.class);
        assignStartActivityActionToView(R.id.btnPulsingCircle, PulsingCircleActivity.class);
        assignStartActivityActionToView(R.id.btnStaticGraph, StaticGraphActivity.class);
    }

    private void assignStartActivityActionToView(@IdRes int viewId, final Class clazz) {
        findViewById(viewId).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivity(new Intent(MainActivity.this, clazz));
            }
        });
    }
}
