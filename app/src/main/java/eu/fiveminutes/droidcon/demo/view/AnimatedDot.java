package eu.fiveminutes.droidcon.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;

import java.util.concurrent.TimeUnit;

public final class AnimatedDot extends View {

    private static final long UI_REFRESH_RATE = 100L; // fps
    private static final long ANIMATION_REFRESHING_INTERVAL = TimeUnit.SECONDS.toMillis(1L) / UI_REFRESH_RATE; // millis
    private static final long ANIMATION_DURATION_IN_MILLIS = 1500L; // millis
    private static final long NUMBER_OF_FRAMES = ANIMATION_DURATION_IN_MILLIS / ANIMATION_REFRESHING_INTERVAL;

    private final Handler uiHandler = new Handler(Looper.getMainLooper());

    private Paint dotPaint;
    private Paint endPointPaint;

    private PointF startPoint;
    private PointF endPoint;
    private float pathStepX;
    private float pathStepY;

    private boolean isAnimating = false;
    private boolean goingStartToEnd = true;

    private PointF[] frames;
    private int currentFrame;

    public AnimatedDot(final Context context) {
        super(context);
        init();
    }

    public AnimatedDot(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimatedDot(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        dotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        dotPaint.setColor(Color.RED);
        dotPaint.setStyle(Paint.Style.FILL);
        dotPaint.setStrokeWidth(1.0f);

        endPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        endPointPaint.setColor(Color.GREEN);
        endPointPaint.setStyle(Paint.Style.FILL);
        endPointPaint.setStrokeWidth(1.0f);
    }

    public void toggle() {
        if (isAnimating) {
            stopAnimating();
        } else {
            startAnimating();
        }

        isAnimating = !isAnimating;
    }

    private void startAnimating() {
        calculateFrames();
        uiHandler.post(invalidateUI);
    }

    private void stopAnimating() {
        uiHandler.removeCallbacks(invalidateUI);
    }

    private Runnable invalidateUI = new Runnable() {

        @Override
        public void run() {
            if (hasFrameToDraw()) {
                invalidate();
                uiHandler.postDelayed(this, ANIMATION_REFRESHING_INTERVAL);
            } else {
                isAnimating = false;
            }
        }
    };

    private void calculateFrames() {

        if (hasFrameToDraw()) {
            return;
        }

        frames = new PointF[(int) NUMBER_OF_FRAMES + 1]; // Damn

        final PointF animationStartPoint;
        final PointF animationEndPoint;
        final float xStep;
        final float yStep;

        if (goingStartToEnd) {
            animationStartPoint = startPoint;
            animationEndPoint = endPoint;
            xStep = pathStepX;
            yStep = -pathStepY;
        } else {
            animationStartPoint = endPoint;
            animationEndPoint = startPoint;
            xStep = -pathStepX;
            yStep = pathStepY;
        }

        float x = animationStartPoint.x;
        float y = animationStartPoint.y;
        for (int i = 0; i < NUMBER_OF_FRAMES; i++) {
            frames[i] = new PointF(x, y);
            x += xStep;
            y += yStep;
        }

        frames[frames.length - 1] = new PointF(animationEndPoint.x, animationEndPoint.y);

        currentFrame = 0;
        goingStartToEnd = !goingStartToEnd;
    }

    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        startPoint = new PointF(width / 4.0f, height * 3.0f / 4.0f);
        endPoint = new PointF(width * 3.0f / 4.0f, height / 4.0f);

        pathStepX = Math.abs(endPoint.x - startPoint.x) / NUMBER_OF_FRAMES;
        pathStepY = Math.abs(endPoint.y - startPoint.y) / NUMBER_OF_FRAMES;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        drawDot(canvas, startPoint, endPointPaint);
        drawDot(canvas, endPoint, endPointPaint);

        if (!hasFrameToDraw()) {
            drawDot(canvas, startPoint, dotPaint);
            return;
        }

        final PointF currentPoint = frames[currentFrame];
        drawDot(canvas, currentPoint, dotPaint);

        currentFrame++;
    }

    private boolean hasFrameToDraw() {
        return frames != null && currentFrame < frames.length;
    }

    private void drawDot(final Canvas canvas, final PointF point, final Paint paint) {
        canvas.drawCircle(point.x, point.y, 10.0f, paint);
    }
}
